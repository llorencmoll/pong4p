﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyA : MonoBehaviour {

	public float speed;
	public Transform Ballposition;
	private Vector3 iniPos;
	private float move;
	public float limit;

	// Use this for initialization
	void Start() {
		iniPos = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		if (Ballposition.transform.position.y > 0)
		{
			if (Ballposition.transform.position.x > transform.position.x) 
			{
				move = 1;
			} 
			else if (Ballposition.transform.position.x < transform.position.x)
			{
				move = -1;
			}
		}
		transform.Translate (move * speed * Time.deltaTime, 0, 0);

		if (transform.position.x >= limit) 
		{
			transform.position = new Vector3 (limit, transform.position.y, transform.position.z);
		}
		else if (transform.position.x < -limit) 
		{
			transform.position = new Vector3 (-limit, transform.position.y, transform.position.z);
		}
	}
}
